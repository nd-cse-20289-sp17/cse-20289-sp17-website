/* message.c */

#include <stdio.h>
#include <stdlib.h>

union Value {
    char    as_c;
    short   as_s;
    int	    as_i;
    long    as_l;
};

struct Thing {
    union Value v0;
    union Value v1;
    union Value v2;
};

#define DUMP_BITS(_v) \
    for (int i = sizeof(_v)*8 - 1; i >= 0; i--) {\
        if ((i + 1) % 8 == 0) putc(' ', stdout); \
        putc(((_v).as_l & 1UL<<i) ? '1' : '0', stdout); \
    } \
    putc('\n', stdout);

int main(int argc, char *argv[]) {
    struct Thing t = {0};

    t.v0.as_i  = 0x65746166;
    t.v0.as_l |= (544434464UL << 32);

    t.v1.as_l  = (0x6261726FUL << 32);
    t.v1.as_i  = ((673304440UL * 3 + 1));

    t.v2.as_l  = 0xA210000;
    t.v2.as_s  = 25856;
    t.v2.as_c  = 108;

    char *s = (char *)&t;
    for (size_t i = 0; i < sizeof(struct Thing); i++) {
        putc(s[i], stdout);
    }

    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
