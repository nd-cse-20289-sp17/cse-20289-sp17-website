#!/usr/bin/env python2.7

import os
import json

import work_queue

from hulk import permutations

# Functions

def setup_fury_queue():
    # Create Work Queue master with:
    #   1. Random port between 9000 - 9999
    #   2. Project name of hulk-NETID
    #   3. Catalog mode enabled
    name  = 'hulk-{}'.format(os.environ.get('USER', os.popen('whoami').read()))
    queue = work_queue.WorkQueue(work_queue.WORK_QUEUE_RANDOM_PORT, name=name, catalog=True)
    queue.specify_log('fury.log') # Specify Work Queue log location
    return queue

def schedule_hulk_task(length, cache, prefix=''):
    command = './hulk.py -c 2 -l {} -p "{}"'.format(length, prefix)

    if command in cache:
        print 'Skipping {}'.format(command)
        return

    task = work_queue.Task(command)

    # Add source files
    for source in ('hulk.py', 'hashes.txt'):
        task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

    print 'Submitting {}'.format(task.command)

    # Submit tasks
    queue.submit(task)

def load_journal_cache(path):
    cache = set()

    if os.path.exists(path):
        for line in open(path):
            name, value = line.rstrip().split(' ', 1)
            if name == 'CMD':
                cache.add(value)

    return cache

# Main Execution

if __name__ == '__main__':
    # Setup Work Queue Master
    queue = setup_fury_queue()

    # Load journal cache
    journal_cache = load_journal_cache('fury.txt')

    # Schedule tasks for lengths 1 - 7
    for length in range(1, 7):
        schedule_hulk_task(length, journal_cache)

    # Schedule tasks for lengths 6 - 8
    for length in range(7, 9):
        for prefix in permutations(length - 6):
            schedule_hulk_task(6, journal_cache, prefix)

    # Wait for all tasks to complete
    while not queue.empty():
        # Wait for a task to complete
        task = queue.wait()

        # Check if task is valid and if task returned successfully
        if task and task.return_status == 0:
            # Write output of task to stdout
            if len(task.output.strip()):
                print task.output

            # Record output to journal cache and journal output
            with open('fury.txt', 'a') as jf:
                for password in task.output.split():
                    jf.write('PWD {}\n'.format(password))
                jf.write('CMD {}\n'.format(task.command))

            journal_cache.add(task.command)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
